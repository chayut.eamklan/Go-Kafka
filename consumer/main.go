package main

import (
	"fmt"
	"log"

	"gitlab.com/chayut.eamklan/Go-Kafka/models"
	"gitlab.com/chayut.eamklan/Go-Kafka/utils"
)

func main() {
	config := models.KafkaConfig{
		URL:   "localhost:9092",
		Topic: "login",
	}

	conn := utils.KafkaConn(config)
	for {
		message, err := conn.ReadMessage(10e6)
		if err != nil {
			log.Fatalf("error while read message: cause by %v", err)
			break
		}

		fmt.Println(string(message.Value))
	}
	
	if err := conn.Close(); err != nil {
		log.Fatalf("error while close connection: cause by %v", err)
	}
}
