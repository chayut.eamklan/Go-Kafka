package main

import (
	"log"
	"time"

	"github.com/segmentio/kafka-go"
	"gitlab.com/chayut.eamklan/Go-Kafka/models"
	"gitlab.com/chayut.eamklan/Go-Kafka/utils"
)

func main() {
	config := models.KafkaConfig{
		URL:   "localhost:9092",
		Topic: "login",
	}

	conn := utils.KafkaConn(config)

	if !utils.IsTopicAlreadyExists(conn, config.Topic) {
		topicConfigs := []kafka.TopicConfig{
			{
				Topic:             config.Topic,
				NumPartitions:     1,
				ReplicationFactor: 1,
			},
		}

		err := conn.CreateTopics(topicConfigs...)
		if err != nil {
			log.Fatalf("error while create topic. cause by : %v", err)
		}
	}

	data := func() []kafka.Message {
		users := []models.User{
			{
				Name:  "chayut",
				Phone: "0988877574",
				Age:   26,
			},
			{
				Name:  "chonticha",
				Phone: "0976563733",
				Age:   22,
			},
		}

		message := make([]kafka.Message, 0)
		for _, v := range users {
			message = append(message, kafka.Message{
				Value: utils.CompressToJson(v),
			})
		}
		return message
	}()

	conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	_, err := conn.WriteMessages(data...)
	if err != nil {
		log.Fatal("failed to write messages:", err)
	}

	if err := conn.Close(); err != nil {
		log.Fatal("failed to close writer:", err)
	}
}
