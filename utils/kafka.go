package utils

import (
	"context"
	"log"

	kafka "github.com/segmentio/kafka-go"
	"gitlab.com/chayut.eamklan/Go-Kafka/models"
)

func KafkaConn(config models.KafkaConfig) *kafka.Conn {
	partition := 0

	conn, err := kafka.DialLeader(context.Background(), "tcp", config.URL, config.Topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	return conn
}

func IsTopicAlreadyExists(conn *kafka.Conn, topic string) bool {
	partitions, err := conn.ReadPartitions()

	if err != nil {
		log.Fatalf("error!! while read partition. cause by : %v", err)
	}

	for _, p := range partitions {
		if p.Topic == topic {
			return true
		}
	}

	return false
}
