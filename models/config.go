package models

type KafkaConfig struct {
	URL string
	Topic    string
}

type ZookeeperConfig struct {
	URL string
	Port         string
}
