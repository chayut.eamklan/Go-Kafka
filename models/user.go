package models

type User struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
	Age   uint8  `json:"age"`
}
